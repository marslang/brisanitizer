<?php
namespace brisanitizer;

use InvalidArgumentException;

class Sanitizer
{
 

    public function __construct()
    {

    }  
    
    public function sanitizeField($value, $sanitizer){   
        //echo 'Brisanitizer->sanitizeField ' . $value . ' - ' .  $sanitizer . "\n";   

        $sanitizedValue = $value;
        // Field exists in schema, so validate accordingly
        //if (!isset($fieldParameters['sanitizers']) || empty($fieldParameters['sanitizers'])) {
        //    return $this->purgeHtmlCharacters($sanitizedValue);
        //} else {
            $processed = false;
            //foreach ($fieldParameters['sanitizers'] as $sanitizer => $attributes){
                switch (strtolower($sanitizer)){
                    //case "purify": $sanitizedValue = $this->_purifier->purify($sanitizedValue); $processed = true; break;
                    case "nospaces": $sanitizedValue = $this->nospaces($sanitizedValue); $processed = true; break;    
                    case "escape": $sanitizedValue = $this->escapeHtmlCharacters($sanitizedValue); $processed = true; break;    
                    case "purge" : $sanitizedValue = $this->purgeHtmlCharacters($sanitizedValue); $processed = true; break;
                    case "raw" : $processed = true; break;
                    default: break;
                }
            //}
            // If no sanitizers have been applied, then apply purge by default
            if (!$processed)
                $sanitizedValue = $this->purgeHtmlCharacters($sanitizedValue);
            return $sanitizedValue;
        //}   
    }       


    private function nospaces($value){
        if (is_array($value)){
            foreach($value as $k => $v){ 
                $value[$k] = str_replace(' ', '', $value[$k]);
            }
            return $value;
        }else{
            return str_replace(' ', '', $value);            
        }
    }   
     
    /** Autodetect if a field is an array or scalar, and filter appropriately. */
    private function escapeHtmlCharacters($value){
        if (is_array($value))
            return filter_var_array($value, FILTER_SANITIZE_SPECIAL_CHARS);
        else
            return filter_var($value, FILTER_SANITIZE_SPECIAL_CHARS);
    }
    
    /** Autodetect if a field is an array or scalar, and filter appropriately. */
    private function purgeHtmlCharacters($value){
        if (is_array($value))
            return filter_var_array($value, FILTER_SANITIZE_STRING);
        else
            return filter_var($value, FILTER_SANITIZE_STRING);
    }   
     
}
